package cn.mhome.spring.thrift.server.config;

public enum FilePathConfig {
	
	DEFAULT_SERVER_IPPORT("/easythrift/config/configcenter.properties","服务的ip和port配置文件");
	
	private FilePathConfig(String path,String msg){
		
	}
	
	private String path;
	private String msg;
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
