package cn.mhome.easy.spring.thrift.client;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class ProxyFactory<T> {
	
	@SuppressWarnings("unchecked")
	public T createProxy(Class<?> service)
			throws Exception {

		Class<?> iFaceClass = Class.forName(service.getName() + "$Iface");
		
		InvocationHandler handler = new ThriftProxy(service.getName());

		Class<?> clientClass = Class.forName(service.getName() + "$Client");
		
		Object proxy = Proxy.newProxyInstance(iFaceClass.getClassLoader(),
				clientClass.getInterfaces(), handler);
		
		return (T)proxy;

	}
	
	
	
}
