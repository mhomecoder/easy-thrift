package cn.mhome.easy.spring.thrift.core.exception;

import java.lang.Thread.UncaughtExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 重写多线程默认异常
 * @author lijin
 */
public class SelfUncaughtExceptionHandler implements UncaughtExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(SelfUncaughtExceptionHandler.class);
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		e.printStackTrace();
		logger.error(t.toString(), e);
	}
}
